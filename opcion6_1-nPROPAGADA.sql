﻿/* Ejemplos Presta-Socio */

  /* opcion 6 (n-1) propagada */

-- creando base de datos
DROP DATABASE opcion6;
CREATE DATABASE opcion6;

USE opcion6;

-- creando tablas

-- ejemplar
CREATE TABLE ejemplar(
  -- campos
  cod_ejemplar varchar(25),

  -- claves
  PRIMARY KEY(cod_ejemplar)
);

-- socio
CREATE TABLE socio(
  -- campos
  cod_socio varchar(25),
  ejemplar varchar(25),
  fecha_i date,
  fecha_f date,

  -- claves
  PRIMARY KEY(cod_socio),

  -- claves ajenas
  CONSTRAINT FKsocioejemplar FOREIGN KEY(ejemplar)
  REFERENCES ejemplar(cod_ejemplar)
);


-- insertando datos en tabla ejemplar
INSERT ejemplar (cod_ejemplar)
  VALUES ('quijote'),('titanic'),('celestina');

  -- comprobando que ha almacenado los datos
  SELECT * FROM ejemplar;

-- insertando datos en tabla socio
INSERT INTO socio (cod_socio, ejemplar, fecha_i, fecha_f)
  VALUES ('ramon', 'quijote', '2019-06-1', CURDATE());

INSERT INTO socio (cod_socio, ejemplar, fecha_i, fecha_f)
  VALUES ('ramon', 'titanic', '2019-06-1', CURDATE());

INSERT INTO socio (cod_socio, ejemplar, fecha_i, fecha_f)
  VALUES ('rosa', 'celestina', '2019-06-2', CURDATE());
  -- comprobando que ha almacenado los datos
  SELECT * FROM socio;





/*
  CONCLUSIONES:
  Al ser de cardinalidad n-1
  conlleva tener un unique y no permite duplicados

*/