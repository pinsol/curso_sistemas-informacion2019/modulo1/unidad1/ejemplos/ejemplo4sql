﻿/* Ejemplos Presta-Socio */

  /* opcion 5 (n-1) propagada */

-- creando base de datos
DROP DATABASE opcion5;
CREATE DATABASE opcion5;

USE opcion5;

-- creando tablas

-- socio
CREATE TABLE socio(
  -- campos
  cod_socio varchar(25),

  -- claves
  PRIMARY KEY(cod_socio)
);

-- ejemplar
CREATE TABLE ejemplar(
  -- campos
  cod_ejemplar varchar(25),
  socio varchar(25),
  fecha_i date,
  fecha_f date,

  -- claves
  PRIMARY KEY(cod_ejemplar),

  -- claves ajenas
  CONSTRAINT FKejemplarsocio FOREIGN KEY(socio)
  REFERENCES socio(cod_socio)
);


-- insertando datos en tabla socio
INSERT INTO socio (cod_socio)
  VALUES ('ramon'),('rosa');

  -- comprobando que ha almacenado los datos
  SELECT * FROM socio;

-- insertando datos en tabla ejemplar
INSERT INTO ejemplar (cod_ejemplar, socio, fecha_i, fecha_f)
  VALUES ('quijote', 'ramon', '2019-06-1', CURDATE());

INSERT INTO ejemplar (cod_ejemplar, socio, fecha_i, fecha_f)
  VALUES ('titanic', 'ramon', '2019-06-1', CURDATE());

INSERT INTO ejemplar (cod_ejemplar, socio, fecha_i, fecha_f)
  VALUES ('celestina', 'rosa', '2019-06-2', CURDATE());

  -- comprobando que ha almacenado los datos
  SELECT * FROM ejemplar;





/*
  CONCLUSIONES:
  Al ser de cardinalidad n-1
  conlleva tener un unique y no permite duplicados

*/