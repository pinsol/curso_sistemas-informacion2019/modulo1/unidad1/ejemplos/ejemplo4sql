﻿/* Ejemplos Presta-Socio */

  /* opcion 4 (1-1) */

-- creando base de datos
DROP DATABASE opcion3;
CREATE DATABASE opcion3;

USE opcion3;

-- creando tablas

-- ejemplar
CREATE TABLE ejemplar(
  -- campos
  cod_ejemplar varchar(25),

  -- claves
  PRIMARY KEY(cod_ejemplar)  
);

-- socio
CREATE TABLE socio(
  -- campos
  cod_socio varchar(25),

  -- claves
  PRIMARY KEY(cod_socio)
);

-- presta
CREATE TABLE presta(
  -- campos
  ejemplar varchar(25),
  socio varchar(25),
  fecha_i date,
  fecha_f date,

  -- claves
  PRIMARY KEY(ejemplar,socio),
  UNIQUE KEY(ejemplar),
  UNIQUE KEY(socio),

  -- claves ajenas
  CONSTRAINT FKprestaejemplar FOREIGN KEY(ejemplar)
  REFERENCES ejemplar(cod_ejemplar),

  CONSTRAINT FKprestasocio FOREIGN KEY(socio)
  REFERENCES socio(cod_socio)
);

-- insertando datos en tabla ejemplar
INSERT INTO ejemplar (cod_ejemplar)
  VALUES ('quijote'),('titanic');

  -- comprobando que ha almacenado los datos
  SELECT * FROM ejemplar;

-- insertando datos en tabla socio
INSERT INTO socio (cod_socio)
  VALUES ('ramon'),('rosa');

  -- comprobando que ha almacenado los datos
  SELECT * FROM socio;

-- insertando datos en tabla presta
INSERT INTO presta (ejemplar, socio, fecha_i, fecha_f)
  VALUES ('quijote', 'ramon', '2019-06-1', CURDATE());

INSERT INTO presta (ejemplar, socio, fecha_i, fecha_f)
  VALUES ('titanic', 'ramon', '2019-06-1', CURDATE());

INSERT INTO presta (ejemplar, socio, fecha_i, fecha_f)
  VALUES ('quijote', 'rosa', '2019-06-2', CURDATE());

  -- comprobando que ha almacenado los datos
  SELECT * FROM presta;


/*
  CONCLUSIONES:
  Al ser de cardinalidad 1-1
  conlleva tener unique y no permite duplicados
  tanto en socios como en ejemplares

*/